<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

return new \Phalcon\Config(array(

    'database'    => array(
        'adapter'  => 'Mysql',
        'host'     => 'localhost',
        'username' => 'root',
        'password' => '127.q1w2e3',
        'dbname'   => 'api_spas',
        'charset'  => 'utf8',
    ),
    'application' => array(
        'modelsDir'     => APP_PATH . '/models/',
        'migrationsDir' => APP_PATH . '/migrations/',
        'viewsDir'      => APP_PATH . '/views/',
        'helpersDir'    => APP_PATH . '/helpers/',
        'baseUri'       => '/',
    )
));
