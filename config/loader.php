<?php

/**
 * Registering an autoloader
 */
$loader = new \Phalcon\Loader();

$loader->registerDirs(
    array(
        $config->application->modelsDir
    )
);

$loader->registerNamespaces([
    'Helpers' => $config->application->helpersDir
]);

$loader->register();
