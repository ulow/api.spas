<?php
/* *
 * @api             {get} /#STATUS_LIST Response status list
 * @apiDescription  _This is *STUB* to demonstrate Response status list_
 * @apiGroup        Status
 *
 * @apiParam        0       <a name="STATUS_0"></a>   SUCCESS
 * @apiParam        1       <a name="STATUS_1"></a>   FAILED
 * @apiParam        21      <a name="STATUS_21"></a>  COMPANY_NOT_FOUND
 * @apiParam        31      <a name="STATUS_31"></a>  AMBULANCE_NOT_FOUND
 * @apiParam        101     <a name="STATUS_101"></a> COMPANY_MISSING_FIELDS
 * @apiParam        700     <a name="STATUS_700"></a> AUTH_SUCCESS
 * @apiParam        701     <a name="STATUS_701"></a> AUTH_FAILED
 */
define('RESPONSE_SUCCESS', 0);
define('RESPONSE_FAILED', 1);
define('RESPONSE_COMPANY_NOT_FOUND', 21);
define('RESPONSE_AMBULANCE_NOT_FOUND', 31);
define('RESPONSE_CALL_NOT_FOUND', 41);
define('RESPONSE_COMPANY_MISSING_FIELDS', 101);
define('RESPONSE_AUTH_SUCCESS', 700);
define('RESPONSE_AUTH_FAILED', 701);
define('RESPONSE_REGISTER_SUCCESS', 710);
define('RESPONSE_REGISTER_FAILED', 711);