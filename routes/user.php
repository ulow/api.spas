<?php
/**
 * @api               {post} /user/authenticate Authenticate User
 * @apiGroup          User
 *
 * @apiParam {String} login User login.
 * @apiParam {String} password User password.
 *
 * @apiSuccess (Success) {Number} status Response number according to [status list](#api-Status-GetStatus_list).
 * @apiSuccessExample Success-Response:
 *     {
 *       "status": 0
 *     }
 *
 * @apiError (Error)  status  Response number according to [status list](#api-Status-GetStatus_list).
 * @apiError (Error)  errors  Error description `if any`.
 * @apiErrorExample   UserNotFound:
 *     {
 *       "status": 1,
 *       "errors": ["user_not_found"]
 *     }
 * @apiErrorExample   MissingFields:
 *     {
 *       "status": 1,
 *       "error": ["missing_fields"]
 *     }
 */

$app->map('/user/authenticate', function () use ($app) {
    $json = $app->request->getJsonRawBody(true);
    $login = isset($json['login']) ? $json['login'] : $app->request->getPost('login');
    $password = isset($json['password']) ? $json['password'] : $app->request->getPost('password');

    if ($login !== null && $password !== null) {
        $user = User::findFirst([
            'conditions' => 'login = :login: and password = :password:',
            'bind'       => [
                'login' => $login,
                'password' => md5($password)
            ]
        ]);

        if ($user !== false) {
            if ($user->token === null) {
                $user->token = md5(uniqid());
            }
            $user->dt_last_visit = time();
            $user->save();

            return $app->response->setJsonContent([
                'status' => RESPONSE_AUTH_SUCCESS,
                'login'  => $user->login,
                'token'  => $user->token
            ]);
        } else {
            return $app->response->setJsonContent([
                'status' => RESPONSE_AUTH_FAILED,
                'errors' => [
                    'user_not_found',
                    htmlentities($login, ENT_QUOTES, 'UTF-8')
                ]
            ]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_AUTH_FAILED,
            'errors' => ['missing_fields']
        ]);
    }
});

$app->map('/user/register', function () use ($app) {
    $json = $app->request->getJsonRawBody(true);
    $login = isset($json['login']) ? $json['login'] : $app->request->getPost('login');
    $password = isset($json['password']) ? $json['password'] : $app->request->getPost('password');
    $name = isset($json['name']) ? $json['name'] : $app->request->getPost('name');
    $surname = isset($json['surname']) ? $json['surname'] : $app->request->getPost('surname');
    $address = isset($json['address']) ? $json['address'] : $app->request->getPost('address');
    $email = isset($json['email']) ? $json['email'] : $app->request->getPost('email');
    $phone = isset($json['phone']) ? $json['phone'] : $app->request->getPost('phone');

    try {
        $user = new User();
        $user->login = $login;
        $user->password = md5($password);
        $user->name = $name;
        $user->surname = $surname;
        $user->address = $address;
        $user->email = $email;
        $user->phone = $phone;
        $user->token = md5(uniqid());
        $user->dt_created = time();
        $user->dt_last_visit = time();

        if ($user->save()) {
            return $app->response->setJsonContent([
                'status' => RESPONSE_REGISTER_SUCCESS,
                'login'  => $user->login,
                'token'  => $user->token
            ]);
        } else {
            return $app->response->setJsonContent([
                'status' => RESPONSE_REGISTER_FAILED,
                'errors' => explode('::IMPLODE::', implode('::IMPLODE::', $user->getMessages()))
            ]);
        }
    } catch (\Phalcon\Db\Exception $e) {
        return $app->response->setJsonContent([
            'status' => RESPONSE_REGISTER_FAILED,
            'errors' => [
                'db_error',
                $e->getMessage()
            ]
        ]);
    } catch (\Exception $e) {
        return $app->response->setJsonContent([
            'status' => RESPONSE_REGISTER_FAILED,
            'errors' => [
                'unexpected_error',
                $e->getMessage()
            ]
        ]);
    }
});