<?php
/**
 * @api {post} /ambulances/available Get available ambulances
 * @apiSampleRequest https://api.03spas.ru/ambulances/available
 * @apiName Available ambulances
 * @apiGroup Ambulances
 * @apiDescription Gets first 100 available ambulances
 * @apiParam {String} login Login
 * @apiParam {String} token Token
 * @apiParam {String} latitude User latitude
 * @apiParam {String} longitude User longitude
 */
$app->map('/ambulances/available', function () use ($app) {
    $json = $app->request->getJsonRawBody(true);
    $login = isset($json['login']) ? $json['login'] : $app->request->getPost('login');
    $token = isset($json['token']) ? $json['token'] : $app->request->getPost('token');

    $currentLat = isset($json['latitude']) ? $json['latitude'] : $app->request->getPost('latitude');
    $currentLong = isset($json['longitude']) ? $json['longitude'] : $app->request->getPost('longitude');

    if (!empty($login) && !empty($token) && !empty($currentLat) && !empty($currentLong)) {
        $user = User::findFirst([
            'conditions' => 'login = :login: and token = :token:',
            'bind'       => [
                'login' => $login,
                'token' => $token
            ]
        ]);

        if ($user !== false) {
            $user->dt_last_visit = time();
            $user->save();
            $ambulances = Ambulance::getWithDistance($currentLat, $currentLong);

            $ambulanceList = [];
            foreach ($ambulances as $ambulance) {
                $ratio = (false !== $ambulance->rating) ? $ambulance->rating->ratio : 0;
                $ambulanceList[] = [
                    'ambulance' => [
                        'ambulance_id'  => $ambulance->ambulance_id,
                        'company_id'    => $ambulance->company_id,
                        'name'          => $ambulance->name,
                        'latitude'      => $ambulance->latitude,
                        'longitude'     => $ambulance->longitude,
                        'status'        => $ambulance->status,
                        'distance'      => $ambulance->distance,
                        'time_distance' => round((60/50) * $ambulance->distance, 2)
                    ],
                    'company'   => $ambulance->company->toArray(),
                    'rating'    => $ratio
                ];
            }


            return $app->response->setJsonContent([
                'status'     => RESPONSE_SUCCESS,
                'ambulances' => $ambulanceList
            ]);
        } else {
            return $app->response->setJsonContent([
                'status' => RESPONSE_AUTH_FAILED,
                'errors' => [
                    'user_not_found',
                    htmlentities($login, ENT_QUOTES, 'UTF-8')
                ]
            ]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_FAILED,
            'errors' => ['missing_fields' => [
                'login' => empty($login),
                'token' => empty($token),
                'currentLat' => empty($currentLat),
                'currentLong' => empty($currentLong)
            ]]
        ]);
    }
});

$app->map('/ambulances/{ambulance_id:[0-9]+}', function ($ambulance_id) use ($app) {
    $json = $app->request->getJsonRawBody(true);
    $login = isset($json['login']) ? $json['login'] : $app->request->getPost('login');
    $token = isset($json['token']) ? $json['token'] : $app->request->getPost('token');

    if ($login !== null && $token !== null) {
        $user = User::findFirst([
            'conditions' => 'login = :login: and token = :token:',
            'bind'       => [
                'login' => $login,
                'token' => $token
            ]
        ]);

        if ($user !== false) {
            $user->dt_last_visit = time();
            $user->save();

            $ambulance = Ambulance::findFirst([
                'conditions' => 'ambulance_id = :ambulance_id:',
                'bind'       => [
                    'ambulance_id' => $ambulance_id
                ]
            ]);

            if($ambulance !== false) {
                return $app->response->setJsonContent([
                    'status' => RESPONSE_SUCCESS,
                    'ambulance'   => $ambulance->toArray(),
                    'company'   => $ambulance->Company->toArray()
                ]);
            }else{
                return $app->response->setJsonContent([
                    'status' => RESPONSE_AMBULANCE_NOT_FOUND,
                    'errors' => [
                        'ambulance_not_found'
                    ]
                ]);
            }
        } else {
            return $app->response->setJsonContent([
                'status' => RESPONSE_AUTH_FAILED,
                'errors' => [
                    'user_not_found',
                    htmlentities($login, ENT_QUOTES, 'UTF-8')
                ]
            ]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_FAILED,
            'errors' => ['missing_fields']
        ]);
    }
});