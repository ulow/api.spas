<?php

$app->map('/transaction/authenticate', function () use ($app) {
    $json = $app->request->getJsonRawBody(true);
    $login = isset($json['login']) ? $json['login'] : $app->request->getPost('login');
    $password = isset($json['password']) ? $json['password'] : $app->request->getPost('password');

    if ($login !== null && $password !== null) {
        $user = User::findFirst([
            'conditions' => 'login = :login: and password = :password:',
            'bind'       => [
                'login' => $login,
                'password' => md5($password)
            ]
        ]);



        if ($user !== false) {
            if ($user->token === null) {
                $user->token = md5(uniqid());
            }
            $user->dt_last_visit = time();
            $user->save();
            return $app->response->setJsonContent([
                'status' => RESPONSE_AUTH_SUCCESS,
                'token'  => $user->token
            ]);
        } else {
            return $app->response->setJsonContent([
                'status' => RESPONSE_AUTH_FAILED,
                'errors' => ['user_not_found', htmlentities($login, ENT_QUOTES, 'UTF-8')]
            ]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_AUTH_FAILED,
            'errors' => ['missing_fields']
        ]);
    }
});