<?php

use Helpers\Validation\ValidationHelper;

/**
 * @api               {post} /company/authenticate Authenticate Company
 * @apiGroup          Company
 *
 * @apiParam {String} login Company login.
 * @apiParam {String} password Company password.
 *
 * @apiSuccess (Success) {Number} status Response number according to [status list](#api-Status-GetStatus_list).
 * @apiSuccessExample Success-Response:
 *     {
 *       "status": 0
 *     }
 *
 * @apiError (Error)  status  Response number according to [status list](#api-Status-GetStatus_list).
 * @apiError (Error)  errors  Error description `if any`.
 * @apiErrorExample   CompanyNotFound:
 *     {
 *       "status": 1,
 *       "errors": ["company_not_found"]
 *     }
 * @apiErrorExample   MissingFields:
 *     {
 *       "status": 1,
 *       "error": ["missing_fields"]
 *     }
 */
$app->map('/company/authenticate', function () use ($app) {
    $login = $app->request->getPost('login');
    $password = $app->request->getPost('password');

    if ($login !== null && $password !== null) {
        $company = Company::findFirst([
            'conditions' => 'login = :login: and password = :password:',
            'bind'       => [
                'login'    => $login,
                'password' => md5($password)
            ]
        ]);

        if ($company !== false) {
            return $app->response->setJsonContent(['status' => RESPONSE_AUTH_SUCCESS]);
        } else {
            return $app->response->setJsonContent([
                'status' => RESPONSE_AUTH_FAILED,
                'errors' => ['company_not_found']
            ]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_AUTH_FAILED,
            'errors' => ['missing_fields']
        ]);
    }
});

/**
 * @api               {post} /company/add Add Company
 * @apiGroup          Company
 *
 * @apiParam {String} login     Company login.
 * @apiParam {String} password  Company password.
 * @apiParam {String} name      Company name.
 * @apiParam {String} address   Company address.
 * @apiParam {String} phone     Company phone.
 * @apiParam {String} email     Company email.
 *
 * @apiSuccess (Success) {Number} status Response number according to [status list](#api-Status-GetStatus_list).
 * @apiSuccess (Success) {Number} company_id Created Company id.
 * @apiSuccessExample Success-Response:
 *     {
 *       "status": 0,
 *       "company_id": 1,
 *     }
 *
 * @apiError (Error)  {Number} status  Response number according to [status list](#api-Status-GetStatus_list).
 * @apiError (Error)  {Array}  errors  Error description.
 * @apiErrorExample   CompanyNotFound:
 *     {
 *       "status": 1,
 *       "errors": ["company_not_found"]
 * @apiErrorExample   MissingFields:
 *     {
 *       "status": 1,
 *       "errors": {
 *         "login":   ["login is required"],
 *         "name":    ["name is required"],
 *         "address": ["address is required"],
 *         "phone":   ["phone is required"],
 *         "email":   ["email is required"]
 *       }
 *     }
 */
$app->map('/company/add', function () use ($app) {
    $login = $app->request->getPost('login');
    $password = $app->request->getPost('password');
    $name = $app->request->getPost('name');
    $address = $app->request->getPost('address');
    $phone = $app->request->getPost('phone');
    $email = $app->request->getPost('email');

    $company = new Company();
    $company->login = $login;
    $company->setPassword($password);
    $company->name = $name;
    $company->address = $address;
    $company->phone = $phone;
    $company->email = $email;

    if ($company->save()) {
        return $app->response->setJsonContent([
            'status'     => RESPONSE_SUCCESS,
            'company_id' => $company->company_id
        ]);
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_FAILED,
            'errors' => ValidationHelper::getErrorMessages($company)
        ]);
    }
});

/**
 * @api               {post} /company/:company_id/get Get Company
 * @apiGroup          Company
 *
 * @apiSuccess (Success) {Number} status Response number according to [status list](#api-Status-GetStatus_list).
 * @apiSuccess (Success) {Array} data Company[] _excluding `password`_.
 * @apiSuccessExample Success-Response:
 *     {
 *       "status": 0,
 *       "data": [
 *         {
 *           "company_id": "1",
 *           "login":      "test",
 *           "name":       "Test Company",
 *           "address":    "Testing street 1\/21",
 *           "phone":      "+007212231334144",
 *           "email":      "email@some.test"
 *         }
 *       ]
 *     }
 *
 * @apiError (CompanyNotFound)  {Number} status  Response number according to [status list](#api-Status-GetStatus_list).
 * @apiErrorExample   CompanyNotFound:
 *     {
 *       "status": 21
 *     }
 *
 * @apiError (MissingFields)  {Number} status  Response number according to [status list](#api-Status-GetStatus_list).
 * @apiError (MissingFields)  {Array}  errors  Error description.
 * @apiErrorExample   MissingFields:
 *     {
 *       "status": 1,
 *       "error": {
 *         "company_id": ["company_id is required"]
 *       }
 *     }
 */
$app->map('/company/{company_id:[0-9]+}/get', function ($company_id) use ($app) {
    if ($company_id !== null) {
        $result = Company::getCompany($company_id);
        if (count($result) > 0) {
            return $app->response->setJsonContent([
                'status' => RESPONSE_SUCCESS,
                'data'   => $result->toArray()
            ]);
        } else {
            return $app->response->setJsonContent(['status' => RESPONSE_COMPANY_NOT_FOUND]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_COMPANY_MISSING_FIELDS,
            'errors' => ['company_id' => ['company_id is required']]
        ]);
    }
});

/**
 * @api               {post} /company/:company_id/edit Edit Company
 * @apiGroup          Company
 *
 * @apiSuccess (Success) {Number} status Response number according to [status list](#api-Status-GetStatus_list).
 * @apiSuccessExample Success-Response:
 *     {
 *       "status": 0
 *     }
 *
 * @apiError (CompanyNotFound)  {Number} status  Response number according to [status list](#api-Status-GetStatus_list).
 * @apiErrorExample   CompanyNotFound:
 *     {
 *       "status": 21
 *     }
 *
 * @apiError (MissingFields)  {Number} status  Response number according to [status list](#api-Status-GetStatus_list).
 * @apiError (MissingFields)  {Array}  errors  Error description.
 * @apiErrorExample   MissingFields:
 *     {
 *       "status": 1,
 *       "errors": {
 *         "company_id": ["company_id is required"]
 *       }
 *     }
 */
$app->map('/company/{company_id:[0-9]+}/edit', function ($company_id) use ($app) {
    /*@todo: auth*/
    $login = $app->request->getPost('login');
    $password = $app->request->getPost('password');
    $name = $app->request->getPost('name');
    $address = $app->request->getPost('address');
    $phone = $app->request->getPost('phone');
    $email = $app->request->getPost('email');

    $company = Company::findFirst($company_id);

    if ($login !== null) {
        $company->login = $login;
    }
    if ($password !== null) {
        $company->setPassword($password);
    }
    if ($name !== null) {
        $company->name = $name;
    }
    if ($address !== null) {
        $company->address = $address;
    }
    if ($phone !== null) {
        $company->phone = $phone;
    }
    if ($email !== null) {
        $company->email = $email;
    }

    if ($company->save()) {
        return $app->response->setJsonContent([
            'status' => RESPONSE_SUCCESS
        ]);
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_FAILED,
            'errors' => ValidationHelper::getErrorMessages($company)
        ]);
    }
});

/**
 * @api               {post} /company/:company_id/delete Delete Company
 * @apiGroup          Company
 *
 * @apiSuccess (Success) {Number} status Response number according to [status list](#api-Status-GetStatus_list).
 * @apiSuccessExample Success-Response:
 *     {
 *         "status": 0
 *     }
 *
 * @apiError    (Error) status  Response number according to [status list](#api-GetStatus_list).
 * @apiError    (Error) errors  Error description `if any`.
 * @apiErrorExample   ValidationError:
 *     {
 *         "status": 1,
 *         "errors": ["error"]
 *     }
 * @apiErrorExample   MissingFields:
 *     {
 *         "status": 101,
 *         "errors": {"company_id": ["company_id is required"]}
 *     }
 * @apiErrorExample   CompanyNotFound:
 *     {
 *         "status": 21
 *     }
 */
$app->map('/company/{company_id:[0-9]+}/delete', function ($company_id) use ($app) {
    if ($company_id !== null) {
        $company = Company::findFirst($company_id);
        if ($company !== false) {
            if ($company->delete()) {
                return $app->response->setJsonContent([
                    'status' => RESPONSE_SUCCESS
                ]);
            } else {
                return $app->response->setJsonContent([
                    'status' => RESPONSE_FAILED,
                    'errors' => ValidationHelper::getErrorMessages($company)
                ]);
            }
        } else {
            return $app->response->setJsonContent(['status' => RESPONSE_COMPANY_NOT_FOUND]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_COMPANY_MISSING_FIELDS,
            'errors' => ['company_id' => ['company_id is required']]
        ]);
    }
});

/**
 * @api               {post} /company/:company_id/calls/add Add Call
 * @apiGroup          Company
 *
 * @apiParam    {Number}  ambulance_id Ambulance ID.
 *
 * @apiSuccess (Success) {Number} status  Response number according to [status list](#api-Status-GetStatus_list).
 * @apiSuccess (Success) {Number} call_id Created Call ID.
 * @apiSuccessExample Success-Response:
 *     {
 *         "status": 0,
 *         "call_id": 1
 *     }
 *
 * @apiError    (Error) status  Response number according to [status list](#api-GetStatus_list).
 * @apiError    (Error) errors  Error description `if any`.
 * @apiErrorExample   ValidationError:
 *     {
 *         "status": 1,
 *         "errors": ["error"]
 *     }
 * @apiErrorExample   MissingFields:
 *     {
 *         "status": 101,
 *         "errors": {"company_id": ["company_id is required"]}
 *     }
 */
$app->map('/company/{company_id:[0-9]+}/calls/add', function ($company_id) use ($app) {
    $ambulance_id = $app->request->getPost('ambulance_id');

    if ($company_id !== null) {
        $company = Company::findFirst($company_id);
        if ($company !== false) {
            $ambulances = $company->getRelated('Ambulance', [
                'ambulance_id = :ambulance_id:',
                'bind' => ['ambulance_id' => $ambulance_id]
            ]);

            if (count($ambulances) === 1) {
                $ambulance = $ambulances->getFirst();
                $call = new Call();
                $call->company_id = $company->company_id;
                $call->ambulance_id = $ambulance->ambulance_id;
                $call->user_id = 0;
                $call->dt_created = time();
                $call->status = 0;

                if ($call->save()) {
                    return $app->response->setJsonContent([
                        'status'  => RESPONSE_SUCCESS,
                        'call_id' => $call->call_id
                    ]);
                } else {
                    return $app->response->setJsonContent([
                        'status' => RESPONSE_FAILED,
                        'errors' => ValidationHelper::getErrorMessages($call)
                    ]);
                }
            } else {
                return $app->response->setJsonContent(['status' => RESPONSE_AMBULANCE_NOT_FOUND]);
            }
        } else {
            return $app->response->setJsonContent(['status' => RESPONSE_COMPANY_NOT_FOUND]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_COMPANY_MISSING_FIELDS,
            'errors' => ['company_id' => ['company_id is required']]
        ]);
    }
});

/**
 * @api               {post} /company/company_id/calls/get Get Call
 * @apiGroup          Company
 *
 * @apiParam    {Number}  ambulance_id Ambulance ID.
 *
 * @apiSuccess (Success) {Number} status Response number according to [status list](#api-Status-GetStatus_list).
 * @apiSuccess (Success) {Array}  calls  Call[].
 * @apiSuccessExample Success-Response:
 *     {
 *         "status": 0,
 *         "calls":  [
 *             {
 *                 "call_id":      "2",
 *                 "company_id":   "1",
 *                 "ambulance_id": "7",
 *                 "user_id":      "1",
 *                 "dt_created":   "1440239517",
 *                 "status":       "1"
 *             }
 *         ]
 *     }
 *
 * @apiError    (Error) status  Response number according to [status list](#api-GetStatus_list).
 * @apiError    (Error) errors  Error description `if any`.
 * @apiErrorExample   AmbulanceNotFound:
 *     {
 *         "status": 31
 *     }
 * @apiErrorExample   CompanyNotFound:
 *     {
 *         "status": 21
 *     }
 */
$app->map('/company/{company_id:[0-9]+}/calls/get', function ($company_id) use ($app) {
    $ambulance_id = $app->request->getPost('ambulance_id');
    if ($company_id !== null) {
        $company = Company::findFirst($company_id);
        if ($company !== false) {
            $ambulances = $company->getRelated('Ambulance', [
                'ambulance_id = :ambulance_id:',
                'bind' => ['ambulance_id' => $ambulance_id]
            ]);

            if (count($ambulances) === 1) {
                $ambulance = $ambulances->getFirst();
                $calls = $ambulance->getRelated('Call');
                return $app->response->setJsonContent([
                    'status' => RESPONSE_SUCCESS,
                    'calls'  => $calls->toArray()
                ]);
            } else {
                return $app->response->setJsonContent(['status' => RESPONSE_AMBULANCE_NOT_FOUND]);
            }
        } else {
            return $app->response->setJsonContent(['status' => RESPONSE_COMPANY_NOT_FOUND]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_COMPANY_MISSING_FIELDS,
            'errors' => ['company_id' => ['company_id is required']]
        ]);
    }
});
$app->map('/company/{company_id:[0-9]+}/ambulances/add', function ($company_id) use ($app) {
    if ($company_id !== null) {
        $company = Company::findFirst($company_id);
        if ($company !== false) {
            $name = $app->request->getPost('name');

            $ambulance = new Ambulance();
            $ambulance->company_id = $company->company_id;
            $ambulance->status = 0;
            $ambulance->name = $name;

            if ($ambulance->save()) {
                return $app->response->setJsonContent([
                    'status'       => RESPONSE_SUCCESS,
                    'ambulance_id' => $ambulance->ambulance_id
                ]);
            } else {
                return $app->response->setJsonContent([
                    'status' => RESPONSE_FAILED,
                    'errors' => ValidationHelper::getErrorMessages($ambulance)
                ]);
            }
        } else {
            return $app->response->setJsonContent(['status' => RESPONSE_COMPANY_NOT_FOUND]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_COMPANY_MISSING_FIELDS,
            'errors' => ['company_id' => ['company_id is required']]
        ]);
    }
});
$app->map('/company/{company_id:[0-9]+}/ambulances/get', function ($company_id) use ($app) {
    if ($company_id !== null) {
        $company = Company::findFirst($company_id);
        if ($company !== false) {
            return $app->response->setJsonContent([
                'status' => RESPONSE_SUCCESS,
                'data'   => $company->Ambulance->toArray()
            ]);
        } else {
            return $app->response->setJsonContent(['status' => RESPONSE_COMPANY_NOT_FOUND]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_COMPANY_MISSING_FIELDS,
            'errors' => ['company_id' => ['company_id is required']]
        ]);
    }
});
$app->map('/company/{company_id:[0-9]+}/ambulance/{ambulance_id:[0-9]+}/get', function ($company_id, $ambulance_id) use ($app) {
    if ($company_id !== null) {
        $company = Company::findFirst($company_id);
        if ($company !== false) {
            return $app->response->setJsonContent([
                'status' => RESPONSE_SUCCESS,
                'data'   => $company->getRelated('Ambulance', [
                    'ambulance_id = :ambulance_id:',
                    'bind' => ['ambulance_id' => $ambulance_id]
                ])->toArray()
            ]);
        } else {
            return $app->response->setJsonContent(['status' => RESPONSE_COMPANY_NOT_FOUND]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_COMPANY_MISSING_FIELDS,
            'errors' => ['company_id' => ['company_id is required']]
        ]);
    }
});
$app->map('/company/{company_id:[0-9]+}/ambulance/{ambulance_id:[0-9]+}/delete', function ($company_id, $ambulance_id) use ($app) {
    if ($company_id !== null) {
        $company = Company::findFirst($company_id);
        if ($company !== false) {
            $ambulance = $company->getRelated('Ambulance', [
                'ambulance_id = :ambulance_id:',
                'bind' => ['ambulance_id' => $ambulance_id]
            ]);
            if (count($ambulance) === 1) {
                if ($ambulance->delete()) {
                    return $app->response->setJsonContent(['status' => RESPONSE_SUCCESS]);
                } else {
                    return $app->response->setJsonContent([
                        'status' => RESPONSE_FAILED,
                        'errors' => ValidationHelper::getErrorMessages($ambulance)
                    ]);
                }
            } else {
                return $app->response->setJsonContent(['status' => RESPONSE_AMBULANCE_NOT_FOUND]);
            }
        } else {
            return $app->response->setJsonContent(['status' => RESPONSE_COMPANY_NOT_FOUND]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_COMPANY_MISSING_FIELDS,
            'errors' => ['company_id' => ['company_id is required']]
        ]);
    }
});
$app->map('/company/{company_id:[0-9]+}/ambulance/{ambulance_id:[0-9]+}/calls/add', function ($company_id, $ambulance_id) use ($app) {
    if ($company_id !== null) {
        $company = Company::findFirst($company_id);
        if ($company !== false) {
            $ambulances = $company->getRelated('Ambulance', [
                'ambulance_id = :ambulance_id:',
                'bind' => ['ambulance_id' => $ambulance_id]
            ]);

            if (count($ambulances) === 1) {
                $ambulance = $ambulances->getFirst();
                $call = new Call();
                $call->company_id = $company->company_id;
                $call->ambulance_id = $ambulance->ambulance_id;
                $call->user_id = 0;
                $call->dt_created = time();
                $call->status = 0;

                if ($call->save()) {
                    return $app->response->setJsonContent([
                        'status'  => RESPONSE_SUCCESS,
                        'call_id' => $call->call_id
                    ]);
                } else {
                    return $app->response->setJsonContent([
                        'status' => RESPONSE_FAILED,
                        'errors' => ValidationHelper::getErrorMessages($call)
                    ]);
                }
            } else {
                return $app->response->setJsonContent(['status' => RESPONSE_AMBULANCE_NOT_FOUND]);
            }
        } else {
            return $app->response->setJsonContent(['status' => RESPONSE_COMPANY_NOT_FOUND]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_COMPANY_MISSING_FIELDS,
            'errors' => ['company_id' => ['company_id is required']]
        ]);
    }
});
$app->map('/company/{company_id:[0-9]+}/ambulance/{ambulance_id:[0-9]+}/calls/get', function ($company_id, $ambulance_id) use ($app) {
    if ($company_id !== null) {
        $company = Company::findFirst($company_id);
        if ($company !== false) {
            $ambulances = $company->getRelated('Ambulance', [
                'ambulance_id = :ambulance_id:',
                'bind' => ['ambulance_id' => $ambulance_id]
            ]);

            if (count($ambulances) === 1) {
                $ambulance = $ambulances->getFirst();
                $calls = $ambulance->getRelated('Call');
                return $app->response->setJsonContent([
                    'status' => RESPONSE_SUCCESS,
                    'calls'  => $calls->toArray()
                ]);
            } else {
                return $app->response->setJsonContent(['status' => RESPONSE_AMBULANCE_NOT_FOUND]);
            }
        } else {
            return $app->response->setJsonContent(['status' => RESPONSE_COMPANY_NOT_FOUND]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_COMPANY_MISSING_FIELDS,
            'errors' => ['company_id' => ['company_id is required']]
        ]);
    }
});