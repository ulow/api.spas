<?php

use Helpers\ApiHelper\ApiHelper;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

$app->map('/admin/authenticate', ApiHelper::adminRequest($app, function(AdminUser $user) use ($app) {
    if ($user->token === null) {
        $user->token = md5(uniqid());
    }
    $user->dt_last_visit = time();
    $user->save();

    return $app->response->setJsonContent([
        'status'   => RESPONSE_AUTH_SUCCESS,
        'login'    => $user->login,
        'token'    => $user->token,
        'userData' => [
            'name'          => $user->name,
            'surname'       => $user->surname,
            'email'         => $user->email,
            'dt_created'    => $user->dt_created,
            'dt_last_visit' => $user->dt_last_visit,
        ]
    ]);
}, 'password'));

$app->map('/admin/company/list/?{page:[0-9]*}', ApiHelper::adminRequest($app, function() use ($app){
    $limit = isset($json['limit']) ? $json['limit'] : $app->request->getPost('limit', null, 1);
    $search = isset($json['search']) ? $json['search'] : $app->request->getPost('search', null, '');

    $page = (int)$app->router->getParams()['page'] > 0 ? (int)$app->router->getParams()['page'] : 1;

    $paginator = new PaginatorModel(
        array(
            "data"  => Company::find([
                'conditions' => '(name like :search: or address like :search: or phone like :search: or email like :search:)',
                'bind'       => [
                    'search' => '%'.$search.'%'
                ]
            ]),
            "limit" => $limit,
            "page"  => $page
        )
    );

    return $app->response->setJsonContent([
        'status' => RESPONSE_SUCCESS,
        'search' => $search,
        'companies' => $paginator->getPaginate()
    ]);
}));

$app->map('/admin/company/{company_id:[0-9]+}/ambulances/list/?{page:[0-9]*}', ApiHelper::adminRequest($app, function() use ($app){
    $limit = isset($json['limit']) ? $json['limit'] : $app->request->getPost('limit', null, 1);

    $page = (int)$app->router->getParams()['page'] > 0 ? (int)$app->router->getParams()['page'] : 1;
    $company_id = (int)$app->router->getParams()['company_id'];

    $paginator = new PaginatorModel(
        array(
            "data"  => Company::findFirst($company_id)->getAmbulance(),
            "limit" => $limit,
            "page"  => $page
        )
    );

    return $app->response->setJsonContent([
        'status' => RESPONSE_SUCCESS,
        'ambulances' => $paginator->getPaginate()
    ]);
}));

$app->map('/admin/test', function () use ($app) {
    print_r($app->request->getRawBody());
    die('test');
    return $app->response->setJsonContent([
        'status' => RESPONSE_SUCCESS,
        'test' => 'test'
    ]);
});
