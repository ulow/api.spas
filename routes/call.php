<?php
/**
 * @api {post} /call/request Request a call
 * @apiSampleRequest https://api.03spas.ru/call/request
 * @apiName Request a call
 * @apiGroup Calls
 * @apiDescription Requests a callback to current user
 * @apiParam {String} login Login
 * @apiParam {String} token Token
 * @apiParam {Integer} ambulance_id Ambulance ID
 */
$app->map('/call/request', function () use ($app) {
    $json = $app->request->getJsonRawBody(true);
    $login = isset($json['login']) ? $json['login'] : $app->request->getPost('login');
    $token = isset($json['token']) ? $json['token'] : $app->request->getPost('token');

    $ambulance_id = isset($json['ambulance_id']) ? $json['ambulance_id'] : $app->request->getPost('ambulance_id');

    if ($login !== null && $token !== null && $ambulance_id !== null) {
        $user = User::findFirst([
            'conditions' => 'login = :login: and token = :token:',
            'bind'       => [
                'login' => $login,
                'token' => $token
            ]
        ]);

        if ($user !== false) {
            $user->dt_last_visit = time();
            $user->save();

            $status = Status::findFirst([
                'conditions' => "type = 'call' and name = 'pending'"
            ]);
            try {
                $call = new Call();
                $call->ambulance_id = $ambulance_id;
                $call->user_id = $user->user_id;
                $call->dt_created = time();
                $call->status = $status->status_id;

                if ($call->save()) {
                    return $app->response->setJsonContent([
                        'status'  => RESPONSE_SUCCESS,
                        'call_id' => $call->call_id
                    ]);
                } else {
                    return $app->response->setJsonContent([
                        'status' => RESPONSE_FAILED,
                        'errors' => explode('::IMPLODE::', implode('::IMPLODE::', $user->getMessages()))
                    ]);
                }
            } catch (\Phalcon\Db\Exception $e) {
                return $app->response->setJsonContent([
                    'status' => RESPONSE_FAILED,
                    'errors' => [
                        'db_error',
                        $e->getMessage()
                    ]
                ]);
            } catch (\Exception $e) {
                return $app->response->setJsonContent([
                    'status' => RESPONSE_FAILED,
                    'errors' => [
                        'unexpected_error',
                        $e->getMessage()
                    ]
                ]);
            }
        } else {
            return $app->response->setJsonContent([
                'status' => RESPONSE_AUTH_FAILED,
                'errors' => [
                    'user_not_found',
                    htmlentities($login, ENT_QUOTES, 'UTF-8')
                ]
            ]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_FAILED,
            'errors' => ['missing_fields']
        ]);
    }
});


/**
 * @api {post} /call/history Get call history
 * @apiSampleRequest https://api.03spas.ru/call/history
 * @apiName Get call history
 * @apiGroup Calls
 * @apiDescription Get call history for current user
 * @apiParam {String} login Login
 * @apiParam {String} token Token
 */
$app->map('/call/history', function () use ($app) {
    $json = $app->request->getJsonRawBody(true);
    $login = isset($json['login']) ? $json['login'] : $app->request->getPost('login');
    $token = isset($json['token']) ? $json['token'] : $app->request->getPost('token');

    if ($login !== null && $token !== null) {
        $user = User::findFirst([
            'conditions' => 'login = :login: and token = :token:',
            'bind'       => [
                'login' => $login,
                'token' => $token
            ]
        ]);

        if ($user !== false) {
            $user->dt_last_visit = time();
            $user->save();

            $calls = Call::find([
                'conditions' => 'user_id = :user_id:',
                'bind'       => [
                    'user_id' => $user->user_id
                ],
                'order' => 'dt_created desc'
            ]);
            $callList = [];
            foreach ($calls as $call) {
                $callList[] = [
                    'call'      => $call->toArray(),
                    'ambulance' => $call->Ambulance->toArray(),
                    'company'   => $call->Ambulance->Company->toArray(),
                    'status'    => $call->Status->name
                ];
            }


            return $app->response->setJsonContent([
                'status' => RESPONSE_SUCCESS,
                'calls'  => $callList
            ]);
        } else {
            return $app->response->setJsonContent([
                'status' => RESPONSE_AUTH_FAILED,
                'errors' => [
                    'user_not_found',
                    htmlentities($login, ENT_QUOTES, 'UTF-8')
                ]
            ]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_FAILED,
            'errors' => ['missing_fields']
        ]);
    }
});

/**
 * @api {post} /call/:call_id Get call by call_id
 * @apiSampleRequest https://api.03spas.ru/call/:call_id
 * @apiName Get call by call_id
 * @apiGroup Calls
 * @apiDescription Get call history for current user
 * @apiParam {Integer} call_id call_id
 * @apiParam {String} login Login
 * @apiParam {String} token Token
 */
$app->map('/call/{call_id:[0-9]+}', function ($call_id) use ($app) {
    $json = $app->request->getJsonRawBody(true);
    $login = isset($json['login']) ? $json['login'] : $app->request->getPost('login');
    $token = isset($json['token']) ? $json['token'] : $app->request->getPost('token');

    if ($login !== null && $token !== null) {
        $user = User::findFirst([
            'conditions' => 'login = :login: and token = :token:',
            'bind'       => [
                'login' => $login,
                'token' => $token
            ]
        ]);

        if ($user !== false) {
            $user->dt_last_visit = time();
            $user->save();

            $call = Call::findFirst([
                'conditions' => 'user_id = :user_id: and call_id = :call_id:',
                'bind'       => [
                    'user_id' => $user->user_id,
                    'call_id' => $call_id
                ]
            ]);

            if($call !== false) {
                return $app->response->setJsonContent([
                    'status' => RESPONSE_SUCCESS,
                    'call'   => $call->toArray()
                ]);
            }else{
                return $app->response->setJsonContent([
                    'status' => RESPONSE_CALL_NOT_FOUND,
                    'errors' => [
                        'call_not_found'
                    ]
                ]);
            }
        } else {
            return $app->response->setJsonContent([
                'status' => RESPONSE_AUTH_FAILED,
                'errors' => [
                    'user_not_found',
                    htmlentities($login, ENT_QUOTES, 'UTF-8')
                ]
            ]);
        }
    } else {
        return $app->response->setJsonContent([
            'status' => RESPONSE_FAILED,
            'errors' => ['missing_fields']
        ]);
    }
});