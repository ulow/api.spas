<?php

use Phalcon\Mvc\Model\Validator\Email as Email;

class Company extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $company_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $address;

    /**
     *
     * @var string
     */
    public $phone;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $avatar;

    /**
     *
     * @var string
     */
    public $avatar_data;

    /**
     * Validations and business logic
     * @return boolean
     */
    public function validation()
    {
        $this->validate(new Email(array(
            'field'    => 'email',
            'required' => true,
        )));
        $this->validate(new \Phalcon\Mvc\Model\Validator\Uniqueness(array(
            'field'   => 'login',
            'message' => 'Provided login already exists',
        )));

        if ($this->validationHasFailed() == true) {
            return false;
        }

        return true;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('company_id', 'Ambulance', 'company_id', array('alias' => 'Ambulance'));
        $this->hasMany('company_id', 'Call', 'company_id', array('alias' => 'Call'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     * @param mixed $parameters
     * @return Company[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     * @param mixed $parameters
     * @return Company
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = md5($password);
    }


    public static function getCompany($company_id)
    {
        return self::query()->columns([
            'company_id',
            'login',
            'name',
            'address',
            'phone',
            'email'
        ])->where('company_id = :company_id:')->bind(['company_id' => $company_id])->execute();
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'company';
    }

}
