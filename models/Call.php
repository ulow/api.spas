<?php

class Call extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $call_id;

    /**
     *
     * @var integer
     */
    public $ambulance_id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var integer
     */
    public $dt_created;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('status', 'Status', 'status_id', array('alias' => 'Status'));
        $this->belongsTo('ambulance_id', 'Ambulance', 'ambulance_id', array('alias' => 'Ambulance'));
        $this->belongsTo('user_id', 'User', 'user_id', array('alias' => 'User'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Call[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Call
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'call';
    }

}
