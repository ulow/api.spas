<?php

class Ambulance extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $ambulance_id;

    /**
     *
     * @var integer
     */
    public $company_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var double
     */
    public $latitude;

    /**
     *
     * @var double
     */
    public $longitude;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     * BEGIN
     *   SET @dist := 6371 * 2 * ASIN(SQRT(
     *       POWER(SIN((src_lat - ABS(dst_lat)) * PI()/180 / 2), 2) +
     *       COS(src_lat * PI()/180) *
     *       COS(ABS(dst_lat) * PI()/180) *
     *       POWER(SIN((src_lon - dst_lon) * PI()/180 / 2), 2)
     *     ));
     *   RETURN round(@dist, 2);
     * END
     *
     * @param int $currentLatitude
     * @param int $currentLongitude
     * @param int $distanceLimit
     * @return Ambulance
     */
    public static function getWithDistance($currentLatitude = 0, $currentLongitude = 0, $distanceLimit = 1000)
    {
        // A raw SQL statement
        $sql = "SELECT *, geodist(" . (float)$currentLatitude . ", " . (float)$currentLongitude . ", `latitude`, `longitude`) as `distance`
        FROM ambulance
        WHERE
            status = 1
            and geodist(" . (float)$currentLatitude . ", " . (float)$currentLongitude . ", `latitude`, `longitude`) < '" . (int)$distanceLimit . "'
        ORDER BY
            distance ASC
        LIMIT 100";

        // Base model
        $ambulance = new Ambulance();

        // Execute the query
        return new \Phalcon\Mvc\Model\Resultset\Simple(null, $ambulance, $ambulance->getReadConnection()->query($sql));
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('ambulance_id', 'Call', 'ambulance_id', array('alias' => 'Call'));
        $this->hasOne('ambulance_id', 'AmbulanceRating', 'ambulance_id', array('alias' => 'Rating'));
        $this->belongsTo('company_id', 'Company', 'company_id', array('alias' => 'Company'));
        $this->belongsTo('status', 'Status', 'status_id', array('alias' => 'Status'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Ambulance[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Ambulance
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'ambulance';
    }

}
