<?php

class AmbulanceRating extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $ambulance_rating_id;

    /**
     *
     * @var integer
     */
    public $ambulance_id;

    /**
     *
     * @var double
     */
    public $ratio;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('ambulance_id', 'Ambulance', 'ambulance_id', array('alias' => 'Ambulance'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AmbulanceRating[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AmbulanceRating
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'ambulance_rating';
    }

}
