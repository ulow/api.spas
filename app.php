<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */

use Helpers\RouteLoader\RouteLoader;
use Phalcon\Mvc\Model\Resultset;

$app->response->setHeader('Access-Control-Allow-Origin', '*');
$app->response->setHeader('Access-Control-Allow-Headers', 'Content-Type');
$app->response->setContentType("application/json", "UTF-8");

RouteLoader::load(APP_PATH . "/routes", $app);

/**
 * Not found handler
 */
$app->notFound(function () use ($app) {
    die(json_encode(['status' => 404]));
});
