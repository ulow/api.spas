define({
  "name": "SPAS API",
  "version": "0.1.0",
  "description": "Simple Private Ambulance Service API",
  "url": "https://api.03spas.ru",
  "template": {
    "withGenerator": false,
    "forceLanguage": "en"
  },
  "sampleUrl": false,
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2015-10-17T14:22:51.428Z",
    "url": "http://apidocjs.com",
    "version": "0.13.1"
  }
});