define({ "api": [
  {
    "type": "post",
    "url": "/ambulances/available",
    "title": "Get available ambulances",
    "sampleRequest": [
      {
        "url": "https://api.03spas.ru/ambulances/available"
      }
    ],
    "name": "Available_ambulances",
    "group": "Ambulances",
    "description": "<p>Gets first 100 available ambulances</p> ",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "login",
            "description": "<p>Login</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "latitude",
            "description": "<p>User latitude</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "longitude",
            "description": "<p>User longitude</p> "
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/ambulances.php",
    "groupTitle": "Ambulances"
  },
  {
    "type": "post",
    "url": "/call/:call_id",
    "title": "Get call by call_id",
    "sampleRequest": [
      {
        "url": "https://api.03spas.ru/call/:call_id"
      }
    ],
    "name": "Get_call_by_call_id",
    "group": "Calls",
    "description": "<p>Get call history for current user</p> ",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>Integer</p> ",
            "optional": false,
            "field": "call_id",
            "description": "<p>call_id</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "login",
            "description": "<p>Login</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p> "
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/call.php",
    "groupTitle": "Calls"
  },
  {
    "type": "post",
    "url": "/call/history",
    "title": "Get call history",
    "sampleRequest": [
      {
        "url": "https://api.03spas.ru/call/history"
      }
    ],
    "name": "Get_call_history",
    "group": "Calls",
    "description": "<p>Get call history for current user</p> ",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "login",
            "description": "<p>Login</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p> "
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/call.php",
    "groupTitle": "Calls"
  },
  {
    "type": "post",
    "url": "/call/request",
    "title": "Request a call",
    "sampleRequest": [
      {
        "url": "https://api.03spas.ru/call/request"
      }
    ],
    "name": "Request_a_call",
    "group": "Calls",
    "description": "<p>Requests a callback to current user</p> ",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "login",
            "description": "<p>Login</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>Integer</p> ",
            "optional": false,
            "field": "ambulance_id",
            "description": "<p>Ambulance ID</p> "
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/call.php",
    "groupTitle": "Calls"
  },
  {
    "type": "post",
    "url": "/company/add",
    "title": "Add Company",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "login",
            "description": "<p>Company login.</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "password",
            "description": "<p>Company password.</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "name",
            "description": "<p>Company name.</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "address",
            "description": "<p>Company address.</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "phone",
            "description": "<p>Company phone.</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "email",
            "description": "<p>Company email.</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-Status-GetStatus_list\">status list</a>.</p> "
          },
          {
            "group": "Success",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "company_id",
            "description": "<p>Created Company id.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"status\": 0,\n  \"company_id\": 1,\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-Status-GetStatus_list\">status list</a>.</p> "
          },
          {
            "group": "Error",
            "type": "<p>Array</p> ",
            "optional": false,
            "field": "errors",
            "description": "<p>Error description.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "CompanyNotFound:",
          "content": "{\n  \"status\": 1,\n  \"errors\": [\"company_not_found\"]",
          "type": "json"
        },
        {
          "title": "MissingFields:",
          "content": "{\n  \"status\": 1,\n  \"errors\": {\n    \"login\":   [\"login is required\"],\n    \"name\":    [\"name is required\"],\n    \"address\": [\"address is required\"],\n    \"phone\":   [\"phone is required\"],\n    \"email\":   [\"email is required\"]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/company.php",
    "groupTitle": "Company",
    "name": "PostCompanyAdd"
  },
  {
    "type": "post",
    "url": "/company/authenticate",
    "title": "Authenticate Company",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "login",
            "description": "<p>Company login.</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "password",
            "description": "<p>Company password.</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-Status-GetStatus_list\">status list</a>.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"status\": 0\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-Status-GetStatus_list\">status list</a>.</p> "
          },
          {
            "group": "Error",
            "optional": false,
            "field": "errors",
            "description": "<p>Error description <code>if any</code>.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "CompanyNotFound:",
          "content": "{\n  \"status\": 1,\n  \"errors\": [\"company_not_found\"]\n}",
          "type": "json"
        },
        {
          "title": "MissingFields:",
          "content": "{\n  \"status\": 1,\n  \"error\": [\"missing_fields\"]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/company.php",
    "groupTitle": "Company",
    "name": "PostCompanyAuthenticate"
  },
  {
    "type": "post",
    "url": "/company/:company_id/calls/add",
    "title": "Add Call",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "ambulance_id",
            "description": "<p>Ambulance ID.</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-Status-GetStatus_list\">status list</a>.</p> "
          },
          {
            "group": "Success",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "call_id",
            "description": "<p>Created Call ID.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"status\": 0,\n    \"call_id\": 1\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-GetStatus_list\">status list</a>.</p> "
          },
          {
            "group": "Error",
            "optional": false,
            "field": "errors",
            "description": "<p>Error description <code>if any</code>.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "ValidationError:",
          "content": "{\n    \"status\": 1,\n    \"errors\": [\"error\"]\n}",
          "type": "json"
        },
        {
          "title": "MissingFields:",
          "content": "{\n    \"status\": 101,\n    \"errors\": {\"company_id\": [\"company_id is required\"]}\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/company.php",
    "groupTitle": "Company",
    "name": "PostCompanyCompany_idCallsAdd"
  },
  {
    "type": "post",
    "url": "/company/company_id/calls/get",
    "title": "Get Call",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "ambulance_id",
            "description": "<p>Ambulance ID.</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-Status-GetStatus_list\">status list</a>.</p> "
          },
          {
            "group": "Success",
            "type": "<p>Array</p> ",
            "optional": false,
            "field": "calls",
            "description": "<p>Call[].</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"status\": 0,\n    \"calls\":  [\n        {\n            \"call_id\":      \"2\",\n            \"company_id\":   \"1\",\n            \"ambulance_id\": \"7\",\n            \"user_id\":      \"1\",\n            \"dt_created\":   \"1440239517\",\n            \"status\":       \"1\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-GetStatus_list\">status list</a>.</p> "
          },
          {
            "group": "Error",
            "optional": false,
            "field": "errors",
            "description": "<p>Error description <code>if any</code>.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "AmbulanceNotFound:",
          "content": "{\n    \"status\": 31\n}",
          "type": "json"
        },
        {
          "title": "CompanyNotFound:",
          "content": "{\n    \"status\": 21\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/company.php",
    "groupTitle": "Company",
    "name": "PostCompanyCompany_idCallsGet"
  },
  {
    "type": "post",
    "url": "/company/:company_id/delete",
    "title": "Delete Company",
    "group": "Company",
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-Status-GetStatus_list\">status list</a>.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"status\": 0\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-GetStatus_list\">status list</a>.</p> "
          },
          {
            "group": "Error",
            "optional": false,
            "field": "errors",
            "description": "<p>Error description <code>if any</code>.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "ValidationError:",
          "content": "{\n    \"status\": 1,\n    \"errors\": [\"error\"]\n}",
          "type": "json"
        },
        {
          "title": "MissingFields:",
          "content": "{\n    \"status\": 101,\n    \"errors\": {\"company_id\": [\"company_id is required\"]}\n}",
          "type": "json"
        },
        {
          "title": "CompanyNotFound:",
          "content": "{\n    \"status\": 21\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/company.php",
    "groupTitle": "Company",
    "name": "PostCompanyCompany_idDelete"
  },
  {
    "type": "post",
    "url": "/company/:company_id/edit",
    "title": "Edit Company",
    "group": "Company",
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-Status-GetStatus_list\">status list</a>.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"status\": 0\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "CompanyNotFound": [
          {
            "group": "CompanyNotFound",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-Status-GetStatus_list\">status list</a>.</p> "
          }
        ],
        "MissingFields": [
          {
            "group": "MissingFields",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-Status-GetStatus_list\">status list</a>.</p> "
          },
          {
            "group": "MissingFields",
            "type": "<p>Array</p> ",
            "optional": false,
            "field": "errors",
            "description": "<p>Error description.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "CompanyNotFound:",
          "content": "{\n  \"status\": 21\n}",
          "type": "json"
        },
        {
          "title": "MissingFields:",
          "content": "{\n  \"status\": 1,\n  \"errors\": {\n    \"company_id\": [\"company_id is required\"]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/company.php",
    "groupTitle": "Company",
    "name": "PostCompanyCompany_idEdit"
  },
  {
    "type": "post",
    "url": "/company/:company_id/get",
    "title": "Get Company",
    "group": "Company",
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-Status-GetStatus_list\">status list</a>.</p> "
          },
          {
            "group": "Success",
            "type": "<p>Array</p> ",
            "optional": false,
            "field": "data",
            "description": "<p>Company[] <em>excluding <code>password</code></em>.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"status\": 0,\n  \"data\": [\n    {\n      \"company_id\": \"1\",\n      \"login\":      \"test\",\n      \"name\":       \"Test Company\",\n      \"address\":    \"Testing street 1\\/21\",\n      \"phone\":      \"+007212231334144\",\n      \"email\":      \"email@some.test\"\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "CompanyNotFound": [
          {
            "group": "CompanyNotFound",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-Status-GetStatus_list\">status list</a>.</p> "
          }
        ],
        "MissingFields": [
          {
            "group": "MissingFields",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-Status-GetStatus_list\">status list</a>.</p> "
          },
          {
            "group": "MissingFields",
            "type": "<p>Array</p> ",
            "optional": false,
            "field": "errors",
            "description": "<p>Error description.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "CompanyNotFound:",
          "content": "{\n  \"status\": 21\n}",
          "type": "json"
        },
        {
          "title": "MissingFields:",
          "content": "{\n  \"status\": 1,\n  \"error\": {\n    \"company_id\": [\"company_id is required\"]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/company.php",
    "groupTitle": "Company",
    "name": "PostCompanyCompany_idGet"
  },
  {
    "type": "get",
    "url": "/#STATUS_LIST",
    "title": "Response status list",
    "description": "<p><em>This is <em>STUB</em> to demonstrate Response status list</em></p> ",
    "group": "Status",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "0",
            "description": "<p><a name=\"STATUS_0\"></a>   SUCCESS</p> "
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "1",
            "description": "<p><a name=\"STATUS_1\"></a>   FAILED</p> "
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "21",
            "description": "<p><a name=\"STATUS_21\"></a>  COMPANY_NOT_FOUND</p> "
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "31",
            "description": "<p><a name=\"STATUS_31\"></a>  AMBULANCE_NOT_FOUND</p> "
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "101",
            "description": "<p><a name=\"STATUS_101\"></a> COMPANY_MISSING_FIELDS</p> "
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "700",
            "description": "<p><a name=\"STATUS_700\"></a> AUTH_SUCCESS</p> "
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "701",
            "description": "<p><a name=\"STATUS_701\"></a> AUTH_FAILED</p> "
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "710",
            "description": "<p><a name=\"STATUS_700\"></a> REGISTER_SUCCESS</p> "
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "711",
            "description": "<p><a name=\"STATUS_701\"></a> REGISTER_FAILED</p> "
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./helpers/ResponseStatus.php",
    "groupTitle": "Status",
    "name": "GetStatus_list"
  },
  {
    "type": "post",
    "url": "/user/authenticate",
    "title": "Authenticate User",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "login",
            "description": "<p>User login.</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "password",
            "description": "<p>User password.</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success": [
          {
            "group": "Success",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-Status-GetStatus_list\">status list</a>.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"status\": 0\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "optional": false,
            "field": "status",
            "description": "<p>Response number according to <a href=\"#api-Status-GetStatus_list\">status list</a>.</p> "
          },
          {
            "group": "Error",
            "optional": false,
            "field": "errors",
            "description": "<p>Error description <code>if any</code>.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "UserNotFound:",
          "content": "{\n  \"status\": 1,\n  \"errors\": [\"user_not_found\"]\n}",
          "type": "json"
        },
        {
          "title": "MissingFields:",
          "content": "{\n  \"status\": 1,\n  \"error\": [\"missing_fields\"]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/user.php",
    "groupTitle": "User",
    "name": "PostUserAuthenticate"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p> "
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./public/doc/main.js",
    "group": "c__server_www_api_spas_ru_public_doc_main_js",
    "groupTitle": "c__server_www_api_spas_ru_public_doc_main_js",
    "name": ""
  }
] });