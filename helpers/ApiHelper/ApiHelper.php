<?php

namespace Helpers\ApiHelper;

use AdminUser;
use Phalcon\Mvc\Micro;

class ApiHelper
{
    /**
     * @param Micro    $app
     * @param callable $callback
     * @param string   $authBy token|password
     * @return callable
     */
    public static function adminRequest($app, $callback, $authBy = 'token')
    {
        return function () use ($app, $callback, $authBy) {
            $json = $app->request->getJsonRawBody(true);
            $login = isset($json['login']) ? $json['login'] : $app->request->getPost('login');

            if ($authBy === 'password') {
                $authToken = isset($json['password']) ? $json['password'] : $app->request->getPost('password');
                $authToken = md5($authToken);
            } else if ($authBy === 'token') {
                $authToken = isset($json['token']) ? $json['token'] : $app->request->getPost('token');
            } else {
                return $app->response->setJsonContent([
                    'status' => -1,
                    'errors' => ['unknown_auth_type']
                ]);
            }

            if ($login !== null && $authToken !== null) {
                $user = AdminUser::findFirst([
                    'conditions' => 'login = :login: and ' . $authBy . ' = :auth_token:',
                    'bind'       => [
                        'login'      => $login,
                        'auth_token' => $authToken
                    ]
                ]);

                if ($user !== false) {
                    $user->dt_last_visit = time();
                    $user->save();
                    return $callback($user, $json);
                } else {
                    return $app->response->setJsonContent([
                        'status' => RESPONSE_AUTH_FAILED,
                        'errors' => [
                            'user_not_found',
                            htmlentities($login, ENT_QUOTES, 'UTF-8')
                        ]
                    ]);
                }
            } else {
                return $app->response->setJsonContent([
                    'status' => RESPONSE_AUTH_FAILED,
                    'errors' => ['missing_fields']
                ]);
            }
        };
    }
}