<?php
namespace Helpers;

/**
 * @api             {get} /#STATUS_LIST Response status list
 * @apiDescription  _This is *STUB* to demonstrate Response status list_
 * @apiGroup        Status
 *
 * @apiParam        0       <a name="STATUS_0"></a>   SUCCESS
 * @apiParam        1       <a name="STATUS_1"></a>   FAILED
 * @apiParam        21      <a name="STATUS_21"></a>  COMPANY_NOT_FOUND
 * @apiParam        31      <a name="STATUS_31"></a>  AMBULANCE_NOT_FOUND
 * @apiParam        101     <a name="STATUS_101"></a> COMPANY_MISSING_FIELDS
 * @apiParam        700     <a name="STATUS_700"></a> AUTH_SUCCESS
 * @apiParam        701     <a name="STATUS_701"></a> AUTH_FAILED
 * @apiParam        710     <a name="STATUS_700"></a> REGISTER_SUCCESS
 * @apiParam        711     <a name="STATUS_701"></a> REGISTER_FAILED
 */
class ResponseStatus {
    const SUCCESS = 0;
    const FAILED = 1;
    const COMPANY_NOT_FOUND = 21;
    const AMBULANCE_NOT_FOUND = 31;
    const CALL_NOT_FOUND = 41;
    const COMPANY_MISSING_FIELDS = 101;
    const AUTH_SUCCESS = 700;
    const AUTH_FAILED = 701;
    const REGISTER_SUCCESS = 710;
    const REGISTER_FAILED = 711;
}