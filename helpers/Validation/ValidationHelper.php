<?php
namespace Helpers\Validation;

class ValidationHelper
{
    public static function getErrorMessages(\Phalcon\Mvc\ModelInterface $model)
    {
        $errors = [];
        foreach ($model->getMessages() as $msg) {
            $errors[$msg->getField()][] = $msg->getMessage();
        }

        return $errors;
    }
}