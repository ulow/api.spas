<?php
namespace Helpers\RouteLoader;

use Phalcon\Mvc\Micro;

class RouteLoader {
    public static function load($path, Micro $app){
        foreach (glob($path . "/*.php") as $filename)
        {
            include_once $filename;
        }
    }
}