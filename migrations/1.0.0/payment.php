<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

class PaymentMigration_100 extends Migration
{

    public function up()
    {
        $this->morphTable(
            'payment',
            array(
            'columns' => array(
                new Column(
                    'payment_id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'autoIncrement' => true,
                        'size' => 10,
                        'first' => true
                    )
                ),
                new Column(
                    'user_id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'size' => 10,
                        'after' => 'payment_id'
                    )
                ),
                new Column(
                    'status',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'size' => 10,
                        'after' => 'user_id'
                    )
                )
            ),
            'indexes' => array(
                new Index('PRIMARY', array('payment_id')),
                new Index('status', array('status')),
                new Index('user_id', array('user_id'))
            ),
            'references' => array(
                new Reference('FK_payment_status', array(
                    'referencedSchema' => 'spas',
                    'referencedTable' => 'status',
                    'columns' => array('status'),
                    'referencedColumns' => array('status_id')
                )),
                new Reference('FK_payment_user', array(
                    'referencedSchema' => 'spas',
                    'referencedTable' => 'user',
                    'columns' => array('user_id'),
                    'referencedColumns' => array('user_id')
                ))
            ),
            'options' => array(
                'TABLE_TYPE' => 'BASE TABLE',
                'AUTO_INCREMENT' => '1',
                'ENGINE' => 'InnoDB',
                'TABLE_COLLATION' => 'utf8_general_ci'
            )
        )
        );
    }
}
