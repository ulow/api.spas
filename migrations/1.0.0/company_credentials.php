<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

class CompanyCredentialsMigration_100 extends Migration
{

    public function up()
    {
        $this->morphTable(
            'company_credentials',
            array(
            'columns' => array(
                new Column(
                    'company_credentials_id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'autoIncrement' => true,
                        'size' => 10,
                        'first' => true
                    )
                ),
                new Column(
                    'company_id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'size' => 10,
                        'after' => 'company_credentials_id'
                    )
                ),
                new Column(
                    'login',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'size' => 50,
                        'after' => 'company_id'
                    )
                ),
                new Column(
                    'password',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'size' => 50,
                        'after' => 'login'
                    )
                )
            ),
            'indexes' => array(
                new Index('PRIMARY', array('company_credentials_id')),
                new Index('company_id', array('company_id')),
                new Index('login', array('login')),
                new Index('password', array('password'))
            ),
            'references' => array(
                new Reference('company', array(
                    'referencedSchema' => 'spas',
                    'referencedTable' => 'company',
                    'columns' => array('company_id'),
                    'referencedColumns' => array('company_id')
                ))
            ),
            'options' => array(
                'TABLE_TYPE' => 'BASE TABLE',
                'AUTO_INCREMENT' => '2',
                'ENGINE' => 'InnoDB',
                'TABLE_COLLATION' => 'utf8_general_ci'
            )
        )
        );
    }
}
