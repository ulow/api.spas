<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

class AmbulanceMigration_100 extends Migration
{

    public function up()
    {
        $this->morphTable(
            'ambulance',
            array(
            'columns' => array(
                new Column(
                    'ambulance_id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'autoIncrement' => true,
                        'size' => 10,
                        'first' => true
                    )
                ),
                new Column(
                    'company_id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'size' => 10,
                        'after' => 'ambulance_id'
                    )
                ),
                new Column(
                    'name',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'notNull' => true,
                        'size' => 200,
                        'after' => 'company_id'
                    )
                ),
                new Column(
                    'latitude',
                    array(
                        'type' => Column::TYPE_DECIMAL,
                        'size' => 9,
                        'scale' => 7,
                        'after' => 'name'
                    )
                ),
                new Column(
                    'longitude',
                    array(
                        'type' => Column::TYPE_DECIMAL,
                        'size' => 9,
                        'scale' => 7,
                        'after' => 'latitude'
                    )
                ),
                new Column(
                    'status',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'size' => 11,
                        'after' => 'longitude'
                    )
                )
            ),
            'indexes' => array(
                new Index('PRIMARY', array('ambulance_id')),
                new Index('company_id', array('company_id')),
                new Index('FK_ambulance_status', array('status')),
                new Index('long_lat', array('latitude', 'longitude'))
            ),
            'references' => array(
                new Reference('FK_ambulance_company', array(
                    'referencedSchema' => 'spas',
                    'referencedTable' => 'company',
                    'columns' => array('company_id'),
                    'referencedColumns' => array('company_id')
                )),
                new Reference('FK_ambulance_status', array(
                    'referencedSchema' => 'spas',
                    'referencedTable' => 'status',
                    'columns' => array('status'),
                    'referencedColumns' => array('status_id')
                ))
            ),
            'options' => array(
                'TABLE_TYPE' => 'BASE TABLE',
                'AUTO_INCREMENT' => '16',
                'ENGINE' => 'InnoDB',
                'TABLE_COLLATION' => 'utf8_general_ci'
            )
        )
        );
    }
}
