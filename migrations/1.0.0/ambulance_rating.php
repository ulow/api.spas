<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

class AmbulanceRatingMigration_100 extends Migration
{

    public function up()
    {
        $this->morphTable(
            'ambulance_rating',
            array(
            'columns' => array(
                new Column(
                    'ambulance_rating_id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'autoIncrement' => true,
                        'size' => 10,
                        'first' => true
                    )
                ),
                new Column(
                    'ambulance_id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'size' => 10,
                        'after' => 'ambulance_rating_id'
                    )
                ),
                new Column(
                    'ratio',
                    array(
                        'type' => Column::TYPE_FLOAT,
                        'notNull' => true,
                        'size' => 1,
                        'after' => 'ambulance_id'
                    )
                )
            ),
            'indexes' => array(
                new Index('PRIMARY', array('ambulance_rating_id')),
                new Index('ambulance_id', array('ambulance_id'))
            ),
            'references' => array(
                new Reference('ambulance', array(
                    'referencedSchema' => 'spas',
                    'referencedTable' => 'ambulance',
                    'columns' => array('ambulance_id'),
                    'referencedColumns' => array('ambulance_id')
                ))
            ),
            'options' => array(
                'TABLE_TYPE' => 'BASE TABLE',
                'AUTO_INCREMENT' => '3',
                'ENGINE' => 'InnoDB',
                'TABLE_COLLATION' => 'utf8_general_ci'
            )
        )
        );
    }
}
