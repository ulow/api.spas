<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

class CallMigration_100 extends Migration
{

    public function up()
    {
        $this->morphTable(
            'call',
            array(
            'columns' => array(
                new Column(
                    'call_id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'autoIncrement' => true,
                        'size' => 10,
                        'first' => true
                    )
                ),
                new Column(
                    'ambulance_id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'size' => 10,
                        'after' => 'call_id'
                    )
                ),
                new Column(
                    'user_id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'size' => 10,
                        'after' => 'ambulance_id'
                    )
                ),
                new Column(
                    'dt_created',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'size' => 10,
                        'after' => 'user_id'
                    )
                ),
                new Column(
                    'status',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'notNull' => true,
                        'size' => 11,
                        'after' => 'dt_created'
                    )
                )
            ),
            'indexes' => array(
                new Index('PRIMARY', array('call_id')),
                new Index('ambulance_id', array('ambulance_id')),
                new Index('user_id', array('user_id')),
                new Index('dt_created', array('dt_created')),
                new Index('status', array('status'))
            ),
            'references' => array(
                new Reference('FK_call_ambulance', array(
                    'referencedSchema' => 'spas',
                    'referencedTable' => 'ambulance',
                    'columns' => array('ambulance_id'),
                    'referencedColumns' => array('ambulance_id')
                )),
                new Reference('FK_call_status', array(
                    'referencedSchema' => 'spas',
                    'referencedTable' => 'status',
                    'columns' => array('status'),
                    'referencedColumns' => array('status_id')
                )),
                new Reference('FK_call_user', array(
                    'referencedSchema' => 'spas',
                    'referencedTable' => 'user',
                    'columns' => array('user_id'),
                    'referencedColumns' => array('user_id')
                ))
            ),
            'options' => array(
                'TABLE_TYPE' => 'BASE TABLE',
                'AUTO_INCREMENT' => '20',
                'ENGINE' => 'InnoDB',
                'TABLE_COLLATION' => 'utf8_general_ci'
            )
        )
        );
    }
}
